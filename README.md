# Installation du projet : 

```
git clone https://gitlab.com/GuilhemFouassier/sosociety.git NOMDUDOSSIERQUEONVEUTCREER
```

Puis se mettre dans le dossier créé et faire : 

```
git pull origin master
```

## Compiler le projet : 

```
npm install
```

## Création du .env :

créer un fichier .env et mettre dedans :

```
PORT=6985
MONGO_URL = 'mongodb://127.0.0.1:27017/society
```

## Démarrer le serveur :

```
npm start
```