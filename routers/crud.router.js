/*
Imports
*/
const express = require('express');
const router = express.Router();
//

/* 
Declarations
*/


//

/*
Routes definition
*/
class CrudRouterClass {
    constructor() {}

    // Set route fonctions
    routes() {
    };

    // Start router
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return router;
    };
};
//

/*
Export
*/
module.exports = CrudRouterClass;
//