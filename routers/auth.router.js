/*
Imports
*/
const express = require('express');
const router = express.Router();
const bcrypt = require('bcryptjs');
//

/* 
Declarations
*/


//

class AuthRouterClass {
    constructor() {}
    routes() {
    }

    
    init() {
        // Get route fonctions
        this.routes();

        // Sendback router
        return router;
    };
}

/*
Export
*/
module.exports = AuthRouterClass;
//